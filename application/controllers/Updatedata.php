<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class updatedata extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'template', 'user_agent'));
        $this->load->js(base_url("assets/app/akademik/student.js"));
        $this->load->model('mstudent', 'student');
    }

    public function login()
    {
        $data = array(
            'title' => 'Sistem Informasi Akademik - STIK Siti khadijah',
        );

        $this->form_validation->set_rules('nim', 'NIM', 'trim|required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('updatedata-cek', $data);
        } else {

            $nim            = $this->input->post('nim');
            $tanggal_lahir  = $this->input->post('tanggal_lahir');
            $cek = $this->_cek_account($nim);

            if ($cek){
                $verifikasi = $this->_verfikasi_account($nim,$tanggal_lahir);
                if ($verifikasi[0]){
                    redirect('updatedata/update/'.$verifikasi[1]);
                }else{
                    $this->template->alert(
                        'Data yang anda masukan tidak cocok.',
                        array('type' => 'warning','icon' => 'times')
                    );
                    redirect('updatedata');
                }

            }else{
                redirect('updatedata/add');
            }
        }
    }

    public function add(){
        $this->template->view('add-mahasiswa');
    }
    /**
     * Take a data type account
     *
     * @param String (username)
     * @access private
     * @return Object
     **/
    private function _cek_account($param = 0)
    {
        $cek = $this->db->query("SELECT student_id FROM students WHERE npm = ?", array($param));

        if ($cek->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    private function _verfikasi_account($nim,$tanggal_lahir)
    {
        $cek = $this->db->query('SELECT * FROM students WHERE npm = "'.$nim.'" AND birts = "'.$tanggal_lahir.'"');

        if ($cek->num_rows() == 1) {
            $id=$cek->row()->student_id;
            return array(true,$id);
        } else {
            return array(false,'');
        }
    }
    /**
     * Handle Create Mahasiswa
     *
     * @return string
     **/
    public function create()
    {
        $this->student->create();

        redirect('updatedata/add');
    }

    /**
     * Halaman Update Mahasiswa
     *
     * @var string
     **/
    public function update($param = 0)
    {
        if($this->student->get($param) == FALSE)
            show_404();


        $this->data = array(
            'title' => "Update data Mahasiswa",
            'js' => $this->load->get_js_files(),
            'get' => $this->student->get($param),
            'dosen_pa' => $this->student->get_all_dosen_pa()
        );

        $this->template->view('update-mahasiswa', $this->data);
    }

    /**
     * handle Update Data Mahasswa
     *
     * @return string
     **/
    public function set_update($param = 0)
    {
        $this->student->update($param);
        redirect("updatedata/update/{$param}");

    }
}

/* End of file Login.php */
/* Location: ./application/modules/Admin/controllers/Login.php */