<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Controller
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Classroom extends Akademik
{
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('akademik'));

		$this->breadcrumbs->unshift(1, 'Master', "akademik/classroom");

		$this->load->model('mclassroom', 'classroom');

        $this->load->model('ex_classroom');

		$this->load->js(base_url("assets/app/akademik/classroom.js"));
	}

	public function index()
	{
		$this->page_title->push('Master', 'Data Ruang Kelas');

		$this->breadcrumbs->unshift(2, 'Data Ruang Kelas', "akademik/classroom");

		$field = array(
			$this->input->get('per_page'),
			$this->input->get('query')
		);

		// set pagination
		$config = $this->template->pagination_list();

		$config['base_url'] = site_url("akademik/classroom?per_page={$field[0]}&query={$field[0]}");

		$config['per_page'] = (!$this->input->get('per_page')) ? 20 : $this->input->get('per_page');
		$config['total_rows'] = $this->classroom->get_all(null, null, 'num');

		$this->pagination->initialize($config);

		$this->data = array(
			'title' => "Data Ruang Kelas",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
			'daftar_ruangan' => $this->classroom->get_all($config['per_page'], $this->input->get('page')),
			'jumlah_kelas' => $config['total_rows']
		);

		$this->template->view('master/data-ruang-kuliah', $this->data);
	}

	public function add()
	{
		$this->page_title->push('Master', 'Tambah Kelas Ruang Baru');

		$this->breadcrumbs->unshift(2, 'Data Ruang Kelas', "akademik/classroom");

		$this->form_validation->set_rules('class_name', 'NIDN', 'trim');
		$this->form_validation->set_rules('name', 'Nama atau Kode Kelas', 'trim');
		$this->form_validation->set_rules('students_limit', 'Kapasitas Kelas', 'trim');
		$this->form_validation->set_rules('description', 'Keterangan', 'trim');

		if($this->form_validation->run() == TRUE)
		{
			$this->classroom->create();

			redirect('akademik/classroom/add');
		}

		$this->data = array(
			'title' => "Data Dosen", 
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
		);

		$this->template->view('master/add-kelas', $this->data);
	}

	public function update($param = 0)
	{
		$this->page_title->push('Master', 'Update Data Kelas');

		$this->breadcrumbs->unshift(2, 'Data Kelas', "akademik/classroom");

        $this->form_validation->set_rules('class_name', 'Nama atau Kode Kelas', 'trim');
        $this->form_validation->set_rules('students_limit', 'Kapasitas Kelas', 'trim');
        $this->form_validation->set_rules('description', 'Keterangan', 'trim');

		if($this->form_validation->run() == TRUE)
		{
			$this->classroom->update($param);

			redirect("akademik/classroom/update/{$param}");
		}

		$this->data = array(
			'title' => "Update Data Kelas",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
			'get' => $this->classroom->get($param)
		);

		$this->template->view('master/update-kelas', $this->data);
	}

	public function delete($param = 0)
	{
		$this->classroom->delete($param);

		redirect("akademik/classroom");
	}

	/**
	 * Multiple Action
	 *
	 * @return string
	 **/
	public function bulk_action()
	{
		switch ($this->input->post('action')) 
		{
			case 'delete':
				$this->classroom->multiple_delete();
				break;
			
			default:
				$this->template->alert(
					' Tidak ada aksi apapun.', 
					array('type' => 'warning','icon' => 'times')
				);
				break;
		}

		redirect('akademik/classroom');
	}

	/**
	 * Cek Validasi Kode Kelas
	 *
	 * @return Bolean
	 **/
	public function validate_dscode()
	{
		if($this->classroom->check_code() == TRUE)
		{
			$this->form_validation->set_message('validate_dscode', 'Maaf! Nama Kelas telah digunakan.');
			return false;
		} else {
			return true;
		}
	}

    /**
     * Get halaman Import Data
     *
     * @return string
     **/
    public function import()
    {
        $this->page_title->push('Ruang Kelas', 'Import Data Kelas');

        $this->breadcrumbs->unshift(2, 'Import Data', "akademik/classroom/add");

        $this->data = array(
            'title' => "Import data kelas",
            'breadcrumb' => $this->breadcrumbs->show(),
            'page_title' => $this->page_title->show(),
            'js' => $this->load->get_js_files(),
        );

        $this->template->view('master/import-ruang-kelas', $this->data);
    }

    public function set_import()
    {
        $this->ex_classroom->set();
    }

    /**
     * Get Download Export
     *
     * @return Attachment
     **/
    public function get_export()
    {
        $this->ex_classroom->get();
    }
}

/* End of file Lecturer.php */
/* Location: ./application/modules/akademik/controllers/Lecturer.php */