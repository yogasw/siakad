<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends Akademik
{
    public $data = array();

    public $thn_akademik;

    public $semester;

    public $student;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('mstudent', 'student');
        $this->load->model('mjadwal', 'jadwal');
        $this->breadcrumbs->unshift(1, 'Jadwal Kuliah', 'mahasiswa/jadwal');

        $this->load->helper('akademik');


        $this->load->js(base_url("assets/app/mahasiswa/jadwal.js?v=1.0.1"));

        $this->ci = $ci =& get_instance();
    }


    public function index()
    {

        $this->breadcrumbs->unshift(2, 'Penyusunan Jadwal', 'mahasiswa/jadwal/create');
        $this->page_title->push('Jadwal Kuliah', 'Penyusunan Jadwal Kuliah ');

        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('npm', 'NPM', 'trim|required');
        $this->form_validation->set_rules('thn_ajaran', 'Tahun Ajaran', 'trim|required');
        $this->form_validation->set_rules('semester', 'Semester', 'trim|required');
        $this->form_validation->run();

        $this->data = array(
            'title' => 'Penyusunan Jadwal Kuliah ',
            'breadcrumb' => $this->breadcrumbs->show(),
            'page_title' => $this->page_title->show(),
            'js' => $this->load->get_js_files(),
            'semester' => $this->semester,
            'thn_akademik' => $this->thn_akademik,
            'mata-kuliah' => $this->jadwal->get_mk()
        );


        $this->template->view('jadwal/susun-jadwal', $this->data);
    }

    /**
     * Simpan Jadwal
     *
     * @param Integer (schedule_id)
     * @return string (Flashdata)
     **/
    public function save($param = 0)
    {
        $this->jadwal->save($param);

        redirect("mahasiswa/jadwal/create?{$this->input->server('QUERY_STRING')}");
    }

    /**
     * Reset Jadwal Mata Kuliah
     *
     * @param Integer (Result_id)
     * @return String
     **/
    public function reset($param = 0)
    {
        $this->jadwal->reset($param);
        redirect("mahasiswa/jadwal/create?{$this->input->server('QUERY_STRING')}");
    }

    public function print_out()
    {
        $this->data = array(
            'title' => 'Penyusunan Jadwal Kuliah ',
            'get' => $this->jadwal->getAll(),
            'mata_kuliah' => $this->jadwal->get_mk()
        );

        $this->load->view('jadwal/print-jadwal', $this->data);
    }

    /**
     * Menampilkan Jadwal by Mata Kuliah
     *
     * @param Integer (course_id)
     * @return String
     **/
    public function getschedule($param = 0)
    {
        if ($this->jadwal->get_schedule($param)) {
            $output = array(
                'status' => TRUE,
            );

            $output['results'] = $this->jadwal->get_schedule($param);
        } else {
            $output = array(
                'status' => FALSE,
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

}

/* End of file Jadwal.php */
/* Location: ./application/modules/mahasiswa/controllers/Jadwal.php */