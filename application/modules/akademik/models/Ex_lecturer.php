<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ex_lecturer extends CI_Model
{
	protected $ci;

	public function __construct()
	{
		parent::__construct();
		$this->ci = $ci =& get_instance();
		// load library
		$this->load->library(array('Excel/PHPExcel', 'upload'));
		$this->load->model('moption', 'option');
		ini_set('max_execution_time', 3000); 
	}
	
	/**
	 * Set Import Data Dosen From Excel
	 *
	 * @package  PHPExcel
	 * @see Documentation (https://github.com/PHPOffice/PHPExcel/wiki)
	 * @return string
	 **/
	public function set()
	{
		$config['upload_path'] = 'assets/excel';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '5120';
		
		$this->upload->initialize($config);
		
		if ( ! $this->upload->do_upload('file_excel')) 
		{
			$output = array('status' => 'ERROR', 'message' => $this->upload->display_errors('<span>','</span>'));
		} else {

			$file_excel = "./assets/excel/{$this->upload->file_name}";

			// Identifikasi File Excel Reader
			try {

				$excelReader = new PHPExcel_Reader_Excel2007();

            	$loadExcel = $excelReader->load($file_excel);	

            	$sheet = $loadExcel->getActiveSheet()->toArray(null, true, true ,true);
		        // Loops Excel data reader

		        foreach ($sheet as $key => $value) 
		        {
		        	// Mulai Dari Baris ketiga
		        	if($key > 3)
		        	{
                        $lecturer = array(
                            'lecturer_code' => $value['B'],
                            'nidn' => $value['C'],
                            'name' => $value['D'],
                            'address' => $value['E'],
                            'phone' => $value['F'],
                            'status' => $value['G'],
                        );

                        $this->db->insert('lecturer', $lecturer);

                        $account = array(
                            'lecturer_id' => $this->db->insert_id(),
                            'email' => '',
                            'password' => password_hash($value['B'], PASSWORD_DEFAULT)
                        );

                        $this->db->insert('lecturer_accounts', $account);

		        	// End Baris ketiga
		        	}
		        // End Loop
		        }

		        unlink("./assets/excel/{$this->upload->file_name}");

				$output = array(
					'status' => 'OK', 
					'message' => ' Data Dosen berhasil diimport.'
				);
			} catch (Exception $e) {
				$output = array(
					'status' => 'ERROR', 
					'message' => 'Error loading file "'.pathinfo($file_excel,PATHINFO_BASENAME).'": '.$e->getMessage()
				);
			}
		}

		echo json_encode($output);
	}

	/**
	 * Cek Validasi Dosen
	 *
	 * @param String = npm 
	 * @return Boolean
	 **/
	public function getNpm($param = 0)
	{
		$query = $this->db->query("SELECT npm FROM students WHERE npm = ?", $param);
		if($query->num_rows())
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}

    /**
     * Export Data Dosen ke Excel
     *
     * @param Array (Dosen Result)
     * @return Attachment excel
     **/
    public function get()
    {
        $objPHPExcel = new PHPExcel();

        $worksheet = $objPHPExcel->createSheet(0);

        for ($cell='A'; $cell <= 'G'; $cell++)
        {
            $worksheet->getStyle($cell.'1')->getFont()->setBold(true);
        }

        $worksheet->getStyle('A1:G1')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'F2F2F2')
                )
            )
        );

        // Header dokumen
        $worksheet->setCellValue('A1', 'NO.')
            ->setCellValue('B1', 'Kode Dosen')
            ->setCellValue('C1', 'NIDN')
            ->setCellValue('D1', 'Nama Dosen')
            ->setCellValue('E1', 'Alamat')
            ->setCellValue('F1', 'Telepon')
            ->setCellValue('G1', 'Status');

        //$this->db->join('concentration', 'course.concentration_id = concentration.concentration_id', 'left');
        $row_cell = 2;
        foreach($this->db->get('lecturer')->result() as $key => $value)
        {
            $worksheet->setCellValue('A'.$row_cell, ++$key)
                ->setCellValue('B'.$row_cell, $value->lecturer_code)
                ->setCellValue('C'.$row_cell, $value->nidn)
                ->setCellValue('D'.$row_cell, $value->name)
                ->setCellValue('E'.$row_cell, $value->address)
                ->setCellValue('F'.$row_cell, $value->phone)
                ->setCellValue('G'.$row_cell, $value->status);
            $row_cell++;
        }

        // Sheet Title
        $worksheet->setTitle("DATA DOSEN");

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');\
        header('Content-Disposition: attachment; filename="DATA-DOSEN.xlsx"');
        $objWriter->save("php://output");
    }
}

/* End of file Ex_student.php */
/* Location: ./application/modules/Akademik/models/Ex_student.php */