<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Model
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Mclassroom extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

	}	

	public function get_all($limit = 20, $offset = 0, $type = 'result')
	{
		if($this->input->get('query') != '')
			$this->db->like('class_name', $this->input->get('query'));

		$this->db->order_by('classroom_id', 'desc');

		if($type == 'result')
		{
			return $this->db->get('classroom', $limit, $offset)->result();
		} else {
			return $this->db->get('classroom')->num_rows();
		}
	}

	public function get($param = '')
	{
		return $this->db->get_where('classroom', array('classroom_id' => $param))->row();
	}
	
	public function create()
	{
		$classroom = array(
			'class_name' => $this->input->post('class_name'),
			'students_limit' => $this->input->post('students_limit'),
			'description' => $this->input->post('description')
		);

		$this->db->insert('classroom', $classroom);

		if($this->db->affected_rows())
		{
			$this->template->alert(
				' Data Kelas ditambahkan.',
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menyimpan data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function update($param = 0)
	{
		$classroom= array(
			'class_name' => $this->input->post('class_name'),
			'students_limit' => $this->input->post('students_limit'),
			'description' => $this->input->post('description'),
		);

		$this->db->update('classroom', $classroom, array('classroom_id' => $param));

		if($this->db->affected_rows())
		{
			$this->template->alert(
				' Perubahan disimpan.', 
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menyimpan data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function delete($param = 0)
	{
		$this->db->delete('classroom', array('classroom_id' => $param));

		if($this->db->affected_rows())
		{
			$this->template->alert(
				' Ruangan terhapus.',
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menghapus data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function multiple_delete()
	{
		if(is_array($this->input->post('classroom')))
		{
			foreach ($this->input->post('classroom') as $key => $value)
			{
				$this->db->delete('classroom', array('classroom_id' => $value));
			}

			if($this->db->affected_rows())
			{
				$this->template->alert(
					' Ruangan terpilih dihapus.',
					array('type' => 'success','icon' => 'check')
				);
			} else {
				$this->template->alert(
					' Gagal menghapus data.', 
					array('type' => 'warning','icon' => 'times')
				);
			}
		}
	}

	/**
	 * Cek Validasi Kode Kelas
	 *
	 * @return Bolean
	 **/
	public function check_code()
	{
		$this->db->where('classroom_id', $this->input->post('classroom_id'));

		return $this->db->get('classroom')->num_rows();
	}
}

/* End of file Mlecturer.php */
/* Location: ./application/modules/akademik/models/Mlecturer.php */