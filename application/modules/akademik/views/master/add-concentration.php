<div class="row">
    <div class="col-md-8 col-md-offset-2 col-xs-12"><?php echo $this->session->flashdata('alert'); ?></div>
    <dov class="col-md-8 col-md-offset-2 col-xs-12">
        <div class="box box-primary">
            <?php
            /**
             * Open Form
             *
             * @var string
             **/
            echo form_open(current_url(), array('class' => 'form-horizontal'));
            ?>
            <div class="box-body" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="concentration_id" class="control-label col-md-3 col-xs-12">ID Konsentrasi : <strong
                                class="text-red">*</strong></label>
                    <div class="col-md-8">
                        <input type="text" name="concentration_id" class="form-control"
                               value="<?php echo set_value('concentration_id'); ?>">
                        <p class="help-block"><?php echo form_error('concentration_id', '<small class="text-red">', '</small>'); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="concentration_name" class="control-label col-md-3 col-xs-12">Nama Konsentrasi : <strong
                                class="text-blue">*</strong></label>
                    <div class="col-md-8">
                        <input type="text" name="concentration_name" class="form-control"
                               value="<?php echo set_value('concentration_name'); ?>">
                        <p class="help-block"><?php echo form_error('concentration_name', '<small class="text-red">', '</small>'); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="concentration_kaprodi" class="control-label col-md-3 col-xs-12">Nama Kaprodi : <strong
                                class="text-blue">*</strong></label>
                    <div class="col-md-8">
                        <input type="text" name="concentration_kaprodi" class="form-control"
                               value="<?php echo set_value('concentration_kaprodi'); ?>">
                        <p class="help-block"><?php echo form_error('concentration_kaprodi', '<small class="text-red">', '</small>'); ?></p>
                    </div>
                </div>
            </div>
            <div class="box-footer with-border">
                <div class="col-md-4 col-xs-5">
                    <a href="<?php echo site_url('akademik/concentration') ?>" class="btn btn-app pull-right">
                        <i class="ion ion-reply"></i> Kembali
                    </a>
                </div>
                <div class="col-md-6 col-xs-6">
                    <button type="submit" class="btn btn-app pull-right">
                        <i class="fa fa-save"></i> Simpan
                    </button>
                </div>
            </div>
            <div class="box-footer with-border">
                <small><strong class="text-red">*</strong> Field wajib diisi!</small>
                <br>
                <small><strong class="text-blue">*</strong> Field Optional</small>
            </div>
            <?php
            // End Form
            echo form_close();
            ?>
        </div>
    </dov>
</div>