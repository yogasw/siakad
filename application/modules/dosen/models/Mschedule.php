<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Jadwal Kuliah Crud Model
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Mschedule extends CI_Model 
{
	public $data;

	public $session_start;

	public $session_end;

	public $semester;

	public $thn_akademik;

    public $userID;

	public function __construct()
	{
		parent::__construct();

		$this->session_start = $this->input->post('session_start')[0].":".$this->input->post('session_start')[1]." ".$this->input->post('session_start')[2];

		$this->session_end = $this->input->post('session_end')[0].":".$this->input->post('session_end')[1]." ".$this->input->post('session_end')[2];

		$this->semester = $this->input->get('semester');

		$this->thn_akademik = $this->input->get('thn_akademik');

		$this->data = array(
			'course_id' => $this->input->post('mk'),
			'lecturer_id' => $this->input->post('lecturer'),
			'classroom_id' => $this->input->post('classroom'),
			'day' => $this->input->post('day'),
			'session_start' => $this->session_start,
			'session_end' => $this->session_end,
			'years' => $this->input->post('thn_akademik'),
			'semester' => $this->input->post('semester') 
		);

		$this->userID = $this->session->userdata('dosen_id');
	}

	public function get_all($limit = 20, $offset = 0, $type = 'result')
	{
		$this->db->select('lecturer_schedule.*, classroom.class_name, course.course_code, course.course_name, course.course_name_english, course.sks, lecturer.lecturer_code, lecturer.name');

		$this->db->join('course', 'lecturer_schedule.course_id = course.course_id', 'left');

		$this->db->join('lecturer', 'lecturer_schedule.lecturer_id = lecturer.lecturer_id', 'left');

		$this->db->join('classroom', 'lecturer_schedule.classroom_id = classroom.classroom_id', 'left');

		$this->db->where('lecturer_schedule.semester', $this->semester)
				 ->where('lecturer_schedule.years', $this->thn_akademik)
		         ->where('lecturer.lecturer_id', $this->userID);

		$this->db->order_by('course.course_name', 'asc');

		if($type == 'result')
		{
			return $this->db->get('lecturer_schedule', $limit, $offset)->result();
		} else {
			return $this->db->get('lecturer_schedule')->num_rows();
		}

	}
}

/* End of file Mschedule.php */
/* Location: ./application/modules/akademik/models/Mschedule.php */