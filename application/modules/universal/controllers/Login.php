<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'template', 'user_agent'));

    }

    public function index()
    {
        $data = array(
            'title' => 'Sistem Informasi Akademik - STIK Siti khadijah',
        );

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $level = array_keys($this->session->userdata());

            if (isset($level[1])) {
                if ($level[1] == 'admin_login') {
                    $this->_redirect("admin");
                } else if ($level[1] == 'dosen_login') {
                    $this->_redirect("dosen");
                } else if ($level[1] == 'is_login') {
                    $this->_redirect("mahasiswa");
                } else {
                    $this->load->view('login-akademik', $data);
                }
            } else {
                $this->load->view('login-akademik', $data);
            }
        } else {

            $username = preg_replace("/[^a-zA-Z0-9]/", "", $this->input->post('username'));
            $password = $this->input->post('password');

            log_app("Login : " . $username);

            if (!isset($username) || !isset($password) || $password == "" || $username == "") {
                $this->template->alert(
                    'Username dan password tidak valid.',
                    array('type' => 'danger', 'icon' => 'times')
                );
                $this->load->view('login-akademik', $data);
                redirect(base_url());
            }
            $type = $this->_get_type_account($username);
            if ($type == "admin") {
                $account = $this->_get_account_admin($username);
            } else if ($type == "dosen") {
                $account = $this->_get_account_dosen($username);
            } else if ($type == "mahasiswa") {
                $account = $this->_get_account_mahasiswa($username);
            } else {
                // set error alert
                $this->template->alert(
                    'Username dan password tidak valid.',
                    array('type' => 'danger', 'icon' => 'times')
                );
                $this->load->view('login-akademik', $data);
                redirect(base_url());
            }
            // authentifaction dengan password verify
            if (password_verify($password, $account->password)) {
                // set session data
                $this->_set_account_login($account, $type);
                $this->_redirect($type);
            } else {
                // set error alert
                $this->template->alert(
                    'Username dan password tidak valid.',
                    array('type' => 'danger', 'icon' => 'times')
                );

                $this->load->view('login-akademik', $data);
            }
        }
    }

    public function _redirect($type)
    {
        if ($type == "admin") {
            redirect('akademik');
        } else if ($type == "dosen") {
            redirect('dosen');
        } else if ($type == "mahasiswa") {
            redirect('mahasiswa');
        } else {
            $this->template->alert(
                'Username dan password tidak valid.',
                array('type' => 'danger', 'icon' => 'times')
            );
            redirect('/');
        }
    }

    /**
     * Take a data type account
     *
     * @param String (username)
     * @access private
     * @return Object
     **/
    private function _get_type_account($param = 0)
    {
        $query_admin = $this->db->query("SELECT username FROM users WHERE username = ?", array($param));
        $query_siswa = $this->db->query("SELECT npm FROM students WHERE npm = ?", array($param));
        $query_dosen = $this->db->query("SELECT lecturer_code FROM lecturer WHERE lecturer_code = ?", array($param));

        if ($query_admin->num_rows() == 1) {
            return "admin";
        } else if ($query_siswa->num_rows() == 1) {
            return "mahasiswa";
        } else if ($query_dosen->num_rows() == 1) {
            return "dosen";
        } else {
            return false;
        }
    }

    /**
     * Take a data admin
     *
     * @param String (username)
     * @access private
     * @return Object
     **/
    private function _get_account_admin($param = 0)
    {
        // get query prepare statmennts
        $query = $this->db->query("
			SELECT user_id, name, username, password, email FROM users WHERE username = ?", array($param));

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            // hilangkan error object
            return (Object)array('password' => '');
        }
    }

    /**
     * Take a data Dosen account
     *
     * @param String (username)
     * @access private
     * @return Object
     **/
    private function _get_account_dosen($param = 0)
    {
        // get query prepare statmennts
        $query = $this->db->query("SELECT lecturer.lecturer_id, lecturer.name, lecturer.lecturer_code, lecturer_accounts.* FROM lecturer JOIN lecturer_accounts ON lecturer.lecturer_id = lecturer_accounts.lecturer_id WHERE lecturer_code = ?", array($param));

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            // hilangkan error object
            return (Object)array('password' => '');
        }
    }

    /**
     * Take a data Mahasiswa account
     *
     * @param Integer (NPM)
     * @access private
     * @return Object
     **/
    private function _get_account_mahasiswa($param = 0)
    {
        // get query prepare statmennts
        $query = $this->db->query("
			SELECT students.npm, students_accounts.* FROM students_accounts 
			LEFT JOIN students ON students.student_id = students_accounts.account_student_id  WHERE students.npm = ?"
            , array($param));

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            // hilangkan error object
            return (Object)array('password' => '');
        }
    }

    /**
     * Create Login Session
     *
     * @param String
     * @access private
     * @return void
     **/
    private function _set_account_login($account, $type)
    {
        if ($type == "admin") {
            $account_session = array(
                'admin_login' => TRUE,
                'user_id' => $account->user_id,
                'account' => $account
            );
        } else if ($type == "dosen") {
            $account_session = array(
                'dosen_login' => TRUE,
                'dosen_id' => $account->lecturer_id,
                'account' => $account
            );
        } else if ($type == "mahasiswa") {
            $account_session = array(
                'is_login' => TRUE,
                'account_id' => $account->account_student_id,
                'account' => $account
            );
        }

        $this->session->set_userdata($account_session);
    }

    /**
     * Sign Out session Destroy
     *
     * @return Void (destroy session)
     **/
    public function signout()
    {
        $this->session->sess_destroy();
        redirect($this->input->get('from_url'));
    }
}

/* End of file Login.php */
/* Location: ./application/modules/Admin/controllers/Login.php */