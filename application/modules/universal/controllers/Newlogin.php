<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newlogin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'template', 'user_agent'));

    }

    public function index()
    {
        $data = array(
            'title' => 'Sistem Informasi Akademik - STIK Siti khadijah',
        );

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('newlogin', $data);
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $type = $this->_get_type_account($username);

            if ($type == "admin") {
                $account = $this->_get_account_admin($username);
            } else if ($type == "dosen") {
                $account = $this->_get_account_dosen($username);
            } else if ($type == "mahasiswa") {
                $account = $this->_get_account_mahasiswa($username);
            } else {
                // set error alert
                $this->template->alert(
                    'Username dan password tidak valid.',
                    array('type' => 'danger', 'icon' => 'times')
                );
                $this->load->view('login-akademik', $data);
                redirect(base_url());
            }
            // authentifaction dengan password verify
            if (password_verify($password, $account->password)) {
                // set session data
                $this->_set_account_login($account, $type);
                if ($type == "admin") {
                    redirect('akademik');
                } else if ($type == "dosen") {
                    redirect('dosen');
                } else if ($type == "mahasiswa") {
                    redirect('mahasiswa');
                } else {
                    redirect('/');
                }
            } else {
                // set error alert
                $this->template->alert(
                    'Username dan password tidak valid.',
                    array('type' => 'danger', 'icon' => 'times')
                );

                $this->load->view('login-akademik', $data);
            }
        }
    }
}

/* End of file Login.php */
/* Location: ./application/modules/Admin/controllers/Login.php */