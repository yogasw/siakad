<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sistem Informasi Akademik - STIK Siti khadijah</title>
    <link rel="stylesheet" href="<?php echo base_url()."assets/style_login.css" ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#3C8DBC">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

</head>
<body>

<div class="wrapper">
    <div class="container">
        <img src=<?php echo base_url()."assets/img/logo_apps.png"?> class="img" alt="logo siakad">

        <form class="form">
            <input type="text" placeholder="NIM atau Kode Dosen ">
            <input type="password" placeholder="Password">
            <button type="submit" id="login-button">Masuk</button>
        </form>
    </div>

    <div class="footer">
            <small>Copyright &copy; 2016 - 2019 <a href="">IT Division</a> STIK Siti khadijah. All rights reserved.</small>
    </div>

    <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
<script src="<?php echo base_url()."assets/login.js" ?>"></script>
</body>
</html>
